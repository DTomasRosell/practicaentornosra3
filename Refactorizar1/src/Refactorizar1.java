/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea de de bloque.
 */


// eliminar los import no utilizados y crear el que se usar� 
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar1 {
	final static String CAD = "Bienvenido al programa";
	// cambiar a mayusculas la constante cad
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		//llamar a la constante despues de cambiarla a mayusculas
		System.out.println(CAD);
		
		System.out.println("Introduce tu dni");
		String dni = scanner.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = scanner.nextLine();
		// darle el valor a la variable en la misma linea y los valores separados
		int numeroA = 7;
		int numeroB = 16;
		int numeroC = 25; 
			
		// separar las condiciones y colocar espacios
		if((numeroA > numeroB) 
		|| (numeroC % 5 != 0 && ((numeroC * 3)- 1)>(numeroB / numeroC))){
		
		// colocamos las llaves detras de los parantesis	
			System.out.println("Se cumple la condici�n");
		}
		
		// separar las variables y colocar parentesis
		numeroC = (numeroA + numeroB) * numeroC + (numeroB / numeroA);
		
		String[] Vector = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
		// colocar los corchetes antes de darle el nombre a la variable del Vector
		// declarar las posiciones de el array al crear el vector
		
		
		Visualizar(Vector);
		
	}
	// cambiar la posicion de[] a antes de la variable para indicar que es String
	static void Visualizar(String[] Vector){
		// colocar espacios y poner llaves despues de los parentesis
		for(int i = 0; i < Vector.length; i ++) {
		
			System.out.println("El dia de la semana en el que te"
			+" encuentras [" + (i+1) + "-7] es el dia: " + Vector[i]);
			
		}
	}
	
}
