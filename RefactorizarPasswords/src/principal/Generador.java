package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		Scanner scanner = MenudeOpciones();
		
		int longitud = LongitudCadena(scanner);
		int opcion = TipodeContraseņa(scanner);
		
		String password = "";
		switch (opcion) {
		case 1:
			password = CaracteresAZ(longitud, password);
			break;
		case 2:
			password = Numeros0a9(longitud, password);
			break;
		case 3:
			password = LetrasCaracteresEspeciales(longitud, password);
			break;
		case 4:
			password = LetrasNumerosCaracteresEspeciales(longitud, password);
			break;
		}

		System.out.println(password);
		scanner.close();
	}
	// Metodo que me selecciona el tipo de contraseņa
	private static int TipodeContraseņa(Scanner scanner) {
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		return opcion;
	}
	// Metodo en el que tienes que introducir una longitud a la cadena
	private static int LongitudCadena(Scanner scanner) {
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		return longitud;
	}
	// Metodo que visualiza el menu de opciones
	private static Scanner MenudeOpciones() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		return scanner;
	}
	// Metodo que calcula una contraseņa con numeros, letras y caracteres especiales
	private static String LetrasNumerosCaracteresEspeciales(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 3);
			if (n == 1) {
				char letra4;
				letra4 = (char) ((Math.random() * 26) + 65);
				password =password + letra4;
			} else if (n == 2) {
				char caracter4;
				caracter4 = (char) ((Math.random() * 15) + 33);
				password += caracter4;
			} else {
				int numero4;
				numero4 = (int) (Math.random() * 10);
				password = password + numero4;
			}
		}
		return password;
	}
	// Metodo que calcula una contraseņa con caracteres especiales
	private static String LetrasCaracteresEspeciales(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 2);
			if (n == 1) {
				char letra3;
				letra3 = (char) ((Math.random() * 26) + 65);
				password += letra3;
			} else {
				char caracter3;
				caracter3 = (char) ((Math.random() * 15) + 33);
				password += caracter3;
			}
		}
		return password;
	}
	// Metodo que calcula una contraseņa con numeros de 0 a 9
	private static String Numeros0a9(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int numero2;
			numero2 = (int) (Math.random() * 10);
			password += numero2;
		}
		return password;
	}

	
	// Metodo que calcula una contraseņa con caracteres de la A a la Z
	private static String CaracteresAZ(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			char letra1;
			letra1 = (char) ((Math.random() * 26) + 65);
			password += letra1;
		}
		return password;
	}

}
