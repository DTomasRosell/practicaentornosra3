/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

// eliminar los import no utilizados y crear el que se usar� 
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar2 {

	static Scanner a;
	public static void main(String[] args) 
	{
	
		a = new Scanner(System.in);
		// cada declaracion en una linea diferente
		int numero;
		int cantidad_maxima_alumnos = 10;
		
		// El [] va pegado al tipo de dato
		int[] arrays = new int[10];
		// Colocamos espacios para que el codigo sea mas limpio
		for(numero=0; numero<10; numero++)
		{
			System.out.println("Introduce nota media de alumno");
			arrays[numero] = a.nextInt();
			// Se le pone nextInt porque pide un numero de tipo Int
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));
		
		// faltan los pentesis que cierran el Scanner
		a.close();
	}
	 // Colocar corchete al lado del tipo de dato
	 // Ponerle un nombre mas especifico al metodo para que sea mas claro
	static double recorrer_array(int[] vector)
	{
		// Hay que especificar el tipo de datos
		// Separar el codigo para que sea mas limpio
		double c = 0;
		for(int a=0; a<10; a++) 
		{
			c=c+vector[a];
		}
		return c/10;
	}
	
}